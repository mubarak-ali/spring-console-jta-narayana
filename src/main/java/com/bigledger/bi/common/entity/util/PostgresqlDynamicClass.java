package com.bigledger.bi.common.entity.util;

import java.sql.SQLException;

import javax.sql.XADataSource;

import org.postgresql.xa.PGXADataSource;

import com.arjuna.ats.internal.jdbc.DynamicClass;
import com.arjuna.ats.jdbc.TransactionalDriver;

public class PostgresqlDynamicClass implements DynamicClass {
   
	@Override
	public XADataSource getDataSource(String dbName) throws SQLException {
		
		return getDataSource(dbName, true);
	}

	private XADataSource getDataSource( String dbName, boolean create ) throws SQLException {
		
		PGXADataSource ds = new PGXADataSource();
		System.out.println( "URL => " + TransactionalDriver.jdbc + dbName );
		
		ds.setUrl( TransactionalDriver.jdbc + dbName );
		return ds;
	}
}
