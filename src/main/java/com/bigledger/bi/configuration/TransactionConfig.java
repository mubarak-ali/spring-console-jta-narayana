package com.bigledger.bi.configuration;

import javax.transaction.SystemException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.jta.JtaTransactionManager;

import com.arjuna.ats.internal.jta.transaction.arjunacore.TransactionManagerImple;
import com.arjuna.ats.internal.jta.transaction.arjunacore.UserTransactionImple;

@Configuration
public class TransactionConfig {

	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager() {

		JtaTransactionManager manager = new JtaTransactionManager();
		manager.setUserTransaction( getUserTransaction() );
		manager.setTransactionManager( getTransactionManager() );
		return manager;
	}
	
	@Bean
	public TransactionManagerImple getTransactionManager()
	{
		TransactionManagerImple tm = new TransactionManagerImple();
		try 
		{
			tm.setTransactionTimeout( 300 );
		} 
		catch (SystemException e) 
		{
			e.printStackTrace();
		}
		return tm;
	}
	
	@Bean 
	public UserTransactionImple getUserTransaction()
	{
		UserTransactionImple utm = new UserTransactionImple();
		try 
		{
			utm.setTransactionTimeout(300);
		} 
		catch (SystemException e) 
		{
			e.printStackTrace();
		}
		return utm;
	}
}
