package com.bigledger.bi.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories( basePackages = "com.bigledger.bi.staging.repository", entityManagerFactoryRef = "stagingEntityManager", transactionManagerRef = "transactionManager" )
@EnableTransactionManagement
public class StagingDBPostgresConfig {

	@Bean(name = "stagingEntityManager")
	public LocalContainerEntityManagerFactoryBean stagingManagerFactory()
	{
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource( stagingDataSource() );
        em.setPackagesToScan( new String[] { "com.bigledger.bi.staging.entity" } );
        
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter( vendorAdapter );
        em.setJpaProperties( additionalProperties() );

        return em;
	}
	
	@Bean
	public DataSource stagingDataSource() 
	{ 
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName( ResourceConfig.getDbStagingDriver() );
        dataSource.setUrl( ResourceConfig.getDbStagingUrl() );
        dataSource.setUsername( ResourceConfig.getDbStagingUsername() );
        dataSource.setPassword( ResourceConfig.getDbStagingPassword() );
        dataSource.setConnectionProperties( databaseProperties() );
        return dataSource;
    }
	
	final Properties databaseProperties()
	{
		final Properties props = new Properties();
		props.setProperty( "DYNAMIC_CLASS", ResourceConfig.getDbStagingDynamicClass() );
        return props;
	}
		   
	final Properties additionalProperties() 
	{
        final Properties props = new Properties();
        props.setProperty( "hibernate.show_sql", ResourceConfig.getHibernateShowSql() );
        props.setProperty( "hibernate.dialect", ResourceConfig.getHibernateDialect() );
        
        props.setProperty( "connection.pool_size", ResourceConfig.getHibernateConnectionPool() );
        props.setProperty( "hibernate.transaction.jta.platform" , "org.hibernate.service.jta.platform.internal.JBossStandAloneJtaPlatform" );
        props.setProperty( "hibernate.transaction.factory_class", "org.hibernate.engine.transaction.internal.jta.CMTTransactionFactory" );
        return props;
    }
	  
}
