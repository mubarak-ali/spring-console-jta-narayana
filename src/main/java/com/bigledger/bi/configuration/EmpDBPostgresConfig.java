package com.bigledger.bi.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories( basePackages = "com.bigledger.bi.emp.repository", entityManagerFactoryRef = "empEntityManager", transactionManagerRef = "transactionManager" )
@EnableTransactionManagement
public class EmpDBPostgresConfig {
	
	@Bean(name = "empEntityManager")
	public LocalContainerEntityManagerFactoryBean empManagerFactory()
	{
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource( empDataSource() );
        em.setPackagesToScan( new String[] { "com.bigledger.bi.emp.entity" } );
        
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter( vendorAdapter );
        em.setJpaProperties( additionalProperties() );

        return em;
	}
	
	@Bean
	public DataSource empDataSource() 
	{ 
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName( ResourceConfig.getDbEmpDriver() );
        dataSource.setUrl( ResourceConfig.getDbEmpUrl() );
        dataSource.setUsername( ResourceConfig.getDbEmpPassword() );
        dataSource.setPassword( ResourceConfig.getDbEmpPassword() );
        dataSource.setConnectionProperties( databaseProperties() );
        return dataSource;
    }
	
	final Properties databaseProperties()
	{
		final Properties props = new Properties();
        props.setProperty( "DYNAMIC_CLASS", ResourceConfig.getDbEmpDynamicClass() );
        return props;
	}
		   
	final Properties additionalProperties() 
	{
        final Properties props = new Properties();
        props.setProperty( "hibernate.show_sql", ResourceConfig.getHibernateShowSql() );
        props.setProperty( "hibernate.dialect", ResourceConfig.getHibernateDialect() );
        
        props.setProperty( "connection.pool_size", ResourceConfig.getHibernateConnectionPool() );
        props.setProperty( "hibernate.transaction.jta.platform" , "org.hibernate.service.jta.platform.internal.JBossStandAloneJtaPlatform" );
        props.setProperty( "hibernate.transaction.factory_class", "org.hibernate.engine.transaction.internal.jta.CMTTransactionFactory" );
        return props;
    }
	  
}
