package com.bigledger.bi.configuration;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan( basePackages={ "com.bigledger.bi.emp.service" } )
public class MainAppConfiguration implements ApplicationContextAware {

	@SuppressWarnings("unused")
	private ApplicationContext applicationContext;
	
	public void setApplicationContext( ApplicationContext applicationContext )
			throws BeansException {
		
		this.applicationContext = applicationContext;
	}

}
