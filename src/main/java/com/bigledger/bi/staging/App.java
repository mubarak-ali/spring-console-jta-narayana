package com.bigledger.bi.staging;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import com.bigledger.bi.configuration.EmpDBPostgresConfig;
import com.bigledger.bi.configuration.MainAppConfiguration;
import com.bigledger.bi.configuration.ResourceConfig;
import com.bigledger.bi.configuration.StagingDBPostgresConfig;
import com.bigledger.bi.configuration.TransactionConfig;
import com.bigledger.bi.emp.service.TestService;

public class App 
{
	public static void main( String[] args )
	{
		final AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

		// setup configuration
		applicationContext.register( MainAppConfiguration.class );
		applicationContext.register( TransactionConfig.class );
		applicationContext.register( EmpDBPostgresConfig.class );
		applicationContext.register( StagingDBPostgresConfig.class );
		applicationContext.register( ResourceConfig.class );
		applicationContext.register( TestService.class );


		// add CLI property source
		applicationContext.getEnvironment().getPropertySources().addLast( new SimpleCommandLinePropertySource( args ) );

		// setup all the dependencies (refresh) and make them run (start)
		applicationContext.refresh();
		applicationContext.start();

		try 
		{
			System.out.println("=========== Welcome to ETL Spring Application ===========");
			TestService service = applicationContext.getBean( TestService.class );
			service.Test();
		} 
		catch (Exception e) {

			System.out.println(e.getMessage());
		} 
		finally {

			applicationContext.close();
		}
	}
}
