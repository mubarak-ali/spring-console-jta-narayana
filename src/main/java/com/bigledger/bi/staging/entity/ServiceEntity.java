package com.bigledger.bi.staging.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "service")
public class ServiceEntity {	

	@Id
	@Column(name = "service_id")
	private int Id;
	
	private String name;

	public int getId() {
		return Id;
	}

	public String getName() {
		return name;
	}

	public void setId(int id) {
		Id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
