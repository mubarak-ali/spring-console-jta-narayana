package com.bigledger.bi.emp.repository;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import com.bigledger.bi.emp.entity.CategoryEntity;

@Repository
public interface ICategoryRepository extends JpaRepository<CategoryEntity, Integer> {
	
	@Override
	@Lock(LockModeType.OPTIMISTIC)
	public CategoryEntity saveAndFlush(CategoryEntity entity);
	
	@Override
	@Lock(LockModeType.OPTIMISTIC)
	public List<CategoryEntity> findAll();
	
	@Override
	@Lock(LockModeType.OPTIMISTIC)
	public Page<CategoryEntity> findAll(Pageable pagable);
	
	@Override
	@Lock(LockModeType.OPTIMISTIC)
	public CategoryEntity findOne(Integer integer);
	
}
