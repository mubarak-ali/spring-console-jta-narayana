package com.bigledger.bi.emp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.bigledger.bi.emp.entity.CategoryEntity;
import com.bigledger.bi.emp.repository.ICategoryRepository;
import com.bigledger.bi.staging.entity.ServiceEntity;
import com.bigledger.bi.staging.repository.IServiceRepository;

public class TestService {
	
	@Autowired
	ICategoryRepository repo;
	
	@Autowired
	IServiceRepository serviceRepo;
	
	@Transactional
	public void Test() {
		
		CategoryEntity entity = new CategoryEntity();
		entity.setDescription("test 5");
		entity.setId(20);
		entity.setName("wavelet 5");
		
		repo.saveAndFlush( entity );
		
		ServiceEntity entityService = new ServiceEntity();
		entityService.setId( 3  );
		entityService.setName("Demo Mubarak 3");
		
		serviceRepo.saveAndFlush( entityService );
		
	}
}
